from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from typing import List
from pydantic import BaseModel
from uuid import uuid4


app = FastAPI()

#cors
origins = [
	"http://localhost:8080"
]

app.add_middleware(
	CORSMiddleware,
	allow_origins=origins,
	allow_credentials=True,
	allow_methods=["*"],
	allow_headers=["*"],
)

class Animais(BaseModel):
	id: int
	name: str
	age: int
	sexy: str
	color: str

banco: List[Animais] = []

@app.get('/animais')
def listarAnimais():
	return banco

@app.get('/animais/{animal_id}')
def obter_animal(animal_id: str):
	for animal in banco:
		if animal.id == animal_id:
			return animal
	return {'erro': 'animal nao localizado'}

@app.delete('/animais/{animal_id}')
def remover_animal(animal_id:str):
	indice = -1
	# buscar a posicao do animal
	for index, animal in enumerate(banco):
		if animal.id == animal_id:
			posicao = index
			break
	if posicao != -1:
		banco.pop(posicao)
		return f'Animal removido com sucesso'
	else:
		return f'Error'

@app.post('/animais')
def cria_animais(animais: Animais):
	animais.id = str(uuid4())
	banco.append(animais)
	return None

